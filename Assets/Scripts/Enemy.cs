﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Enemy : Entity, IDamagable

    {
        [SerializeField]
        private int currentHealth;
        [SerializeField]
        private int attackAmount;
        [SerializeField]
        private bool isAlive;

        public int CurrerntHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }

        public int AttackAmount
        {
            get { return attackAmount; }
            set { attackAmount = value; }
        }

        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }

        public void Damage(int damageAmount)
        {
            if (currentHealth > 0)
            {
                currentHealth -= damageAmount;

                if (currentHealth <= 0)
                {
                    currentHealth = 0;
                    isAlive = false;
                    Log(EntityName + " is dead!" + " /Current health: " + currentHealth);
                    return;
                }
            }
            else if (currentHealth <= 0) 
            {
                currentHealth = 0;
                isAlive = false;
                Log(EntityName + " is dead!" + " /Current health: " + currentHealth);
                return;
            }

                Log(EntityName + " was damaged!" + " /Current health: " + currentHealth);
        }
    }
}