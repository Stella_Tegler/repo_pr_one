﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Player : Entity, IAttack
    {
        [SerializeField]
        private Enemy enemyEntity;

        [SerializeField]
        private int currentHealth;
        [SerializeField]
        private int attackAmount;
        [SerializeField]
        private bool isAlive;

        public Enemy EnemyEntity
        {
            get { return enemyEntity; }
            set { enemyEntity = value; }
        }

        public int CurrerntHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }

        public int AttackAmount
        {
            get { return attackAmount; }
            set { attackAmount = value; }
        }

        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }

        public void AttackEnemy(Enemy enemy, int attackAmount)
        {
            if (enemyEntity.IsAlive == true)
            {
                enemyEntity.Damage(attackAmount);
            }
        }

        void Start()
        {
            Log("Name of Player: " + EntityName);
            Log("Current health of Player: " + currentHealth);
            Log("Attack Amount of Player: " + attackAmount);

            AttackEnemy(enemyEntity, attackAmount);
        }
    }
}