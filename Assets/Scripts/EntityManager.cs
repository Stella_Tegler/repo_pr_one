﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class EntityManager : MonoBehaviour, ILog
    {
        public void Log(object message)
        {
            Debug.Log(message);
        }

        public Entity[] entities;

        void Start()
        {
            Log("Names are being registered");

            for (int i = 0; i < entities.Length; i++)
            {
                Log("Entity at index: " + i + " /has this name: " + entities[i]);
            }
        }
    }
}